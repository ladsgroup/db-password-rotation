import json
import re

from lib import run_sql


with open('omg.json', 'r') as f:
    omg_report = json.load(f)

old_user = 'wikiadmin'
new_user = 'wikiadmin2023'
new_password = 'CHANGEME'

def handle_report(omg_report):
    grants = {}
    for section in omg_report:
        for db in omg_report[section]:
            for user in omg_report[section][db]:
                for grant in omg_report[section][db][user]:
                    grant = grant.replace("'", '`')
                    grants[grant] = grants.get(grant, []) + ['{} ({})'.format(db, section)]
    
    return grants
grants = handle_report(omg_report)


def add_grant(grant, hosts):
    target_host = re.findall(r'`(.+?)`', grant.split(new_user)[1].split('@')[1])[0]
    for host in hosts:
        if 'cloud' in host and host != 'clouddb2001-dev':
            print(host)
            continue
        users = run_sql(host, 'select user,host from mysql.user').split('\n')
        for user in users:
            if not user.strip('') or user.count('\t') != 1:
                continue
            u, h = user.split('\t')
            if u == 'User':
                continue
            if u.strip() == new_user and h.strip() == target_host:
                break
        else:
            run_sql(host, 'CREATE USER IF NOT EXISTS `{}`@`{}` IDENTIFIED BY \'{}\';'.format(new_user, target_host, new_password))
        
        run_sql(host, grant.split('IDENTIFIED BY')[0])


for case in grants:
    if old_user not in case:
        continue
    if new_user in case:
        continue
    new_grant = case.replace(old_user, new_user)
    cases_done = grants.get(new_grant, [])
    cases_not_done = set(grants[case]) - set(cases_done)
    if not cases_not_done:
        continue
    add_grant(new_grant, cases_not_done)
