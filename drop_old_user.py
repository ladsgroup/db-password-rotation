import json
import re
import time

from lib import run_sql

with open('omg.json', 'r') as f:
    omg_report = json.load(f)

old_user = 'wikiadmin'
new_user = 'wikiadmin2023'


def handle_report(omg_report):
    grants = {}
    for section in omg_report:
        for db in omg_report[section]:
            for user in omg_report[section][db]:
                for grant in omg_report[section][db][user]:
                    grant = grant.replace("'", '`')
                    grants[grant] = grants.get(grant, []) + ['{} ({})'.format(db, section)]
    
    return grants
grants = handle_report(omg_report)


def drop_user(grant, hosts):
    target_host = re.findall(r'`(.+?)`', grant.split(old_user)[1].split('@')[1])[0]
    for host in hosts:
        if 'cloud' in host:
            continue
        users = run_sql(host, 'select user,host from mysql.user').split('\n')
        for user in users:
            if not user.strip('') or user.count('\t') != 1:
                continue
            u, h = user.split('\t')
            if u == 'User':
                continue
            if u.strip() == old_user and h.strip() == target_host:
                run_sql(host, 'DROP USER `{}`@`{}`; FLUSH PRIVILEGES;'.format(old_user, target_host))
                time.sleep(1)


for case in grants:
    if old_user not in case or new_user in case:
        continue
    drop_user(case, grants[case])
