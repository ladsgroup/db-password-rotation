# oh my grants
import re
import json

import requests

from lib import run

def handle_section(section):
    res = run("""db-mysql -BN db1215 zarcillo
    -e \"SELECT instances.server, instances.port FROM instances JOIN section_instances
    ON instances.name = section_instances.instance WHERE section_instances.section = '{}'
    ORDER BY instances.name DESC\"""".format(section, section).replace('\n', ' '))
    grants_structured = {}
    for row in res.split('\n'):
        if 'eqiad' not in row and 'codfw' not in row:
            continue
        db = re.split(r'\s+', row.strip())
        db = db[0].split('.')[0] + ':' + db[1]
        users = run('db-mysql {} -e "select user,host from mysql.user;"'.format(db))
        if '\nerror:\n' in users:
            continue
        grants_structured[db] = {}
        for user_row in users.split('\n'):
            if not user_row.strip():
                continue
            user_row = re.split(r'\s+', user_row.strip())
            if len(user_row) == 1:
                user_row.append('')
            (user, domain) = user_row
            if user == 'User' and domain == 'Host':
                continue
            if re.search(r'^[ups]\d+', user) and 'cloud' in db:
                continue
            grants = run('db-mysql {} -e "show grants for \`{}\`@\`{}\`;"'.format(db, user, domain))
            grants = re.sub(r"(IDENTIFIED BY PASSWORD ['\"`]|USING ['\"`]).+(['\"`])", r"\1redacted\2", grants)
            grants_list = []
            for grant_line in grants.split('\n'):
                if not grant_line.strip().startswith('GRANT'):
                    continue
                grants_list.append(grant_line.strip())
            grants_structured[db]['{}@{}'.format(user, domain)] = grants_list
    return grants_structured

full_results = {}
config = requests.get('https://noc.wikimedia.org/dbconfig/eqiad.json').json()
sections = list(config['externalLoads'].keys()) + ['s{}'.format(i+1) for i in range(8)] + ['pc1', 'pc2', 'pc3', 'pc4']
for section in sections:
    full_results[section] = handle_section(section)
    with open('omg.json', 'w') as f:
        f.write(json.dumps(full_results))
