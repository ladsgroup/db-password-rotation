for section in s1 s2 s3 s4 s5 s6 s7 s8 es1 es2 es3 es4 es5 pc1 pc2 pc3 pc4 x1 x2
do
for replica in $(db-mysql -BN db2185 zarcillo -e "SELECT name FROM instances JOIN section_instances ON instances.name = section_instances.instance WHERE section_instances.section IN ('$section', 'test-$section') AND name not in (select instance from masters where section = '$section')  ORDER BY instances.name DESC;")
do
    echo "Doing replica $replica in $section"
    curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_slave.sql?format=TEXT' | base64 -d | db-mysql $replica
    sleep 1
done

for master in $(db-mysql -BN db2185 zarcillo -e "SELECT name FROM instances JOIN section_instances ON instances.name = section_instances.instance WHERE section_instances.section IN ('$section', 'test-$section') AND name in (select instance from masters where section = '$section')  ORDER BY instances.name DESC;")
do
    echo "Doing master, $master in $section"
    curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_master.sql?format=TEXT' | base64 -d | db-mysql $master
    sleep 1
done

done
