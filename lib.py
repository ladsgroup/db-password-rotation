import subprocess
import sys
import time
from datetime import datetime


def run(command):
    if 'IDENTIFIED' not in command:
        print(datetime.fromtimestamp(time.time()), command)
    else:
        print('Creating the user, password hidden')
    if '--run' not in sys.argv and 'show slave' not in command:
        return ''
    return run_internal(command)

def run_internal(command):
    """Do not call this function directly."""
    res = subprocess.run(command, capture_output=True, shell=True)
    stderr = res.stderr.decode('utf-8')
    stderr_cleaned = re.sub(r'\s', '', stderr)
    if stderr_cleaned:
        print('ERROR: ')
        print('--------')
        print(stderr)
        print('--------')
    else:
        stderr = ''
    stdout = res.stdout.decode('utf-8')
    print('STDOUT')
    stdout_cleaned = re.sub(r'\s', '', stdout)
    if stdout_cleaned:
        print(stdout)
    else:
        stdout = ''

    output = stdout
    if stderr_cleaned:
        output += '\nerror:\n' + stderr
    return output


def run_sql(host, sql):
    host = host.split(' ')[0]
    if '"' in sql:
        sql = sql.replace('"', '\\"')
    if '`' in sql:
        sql = sql.replace('`', '\\`')
    if '\n' in sql:
        sql = sql.replace('\n', ' ')
    if not sql.strip().endswith(';'):
        sql += ';'
    return run('db-mysql {} -e "set session sql_log_bin=0; {}"'.format(host, sql))